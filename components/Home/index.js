import React from 'react'
import MainBanner from './MainBanner'
import NewsSection from './NewsSection'
import TabsSection from './TabsSection'

function Home() {
    return(
        <>
        <MainBanner />
        <NewsSection />
        <TabsSection />
        <NewsSection />
        </>
    )
}

export default Home