import React from 'react'
import { Row, Col, Button, Divider, Card } from 'antd';
import { FacebookOutlined, TwitterOutlined, InstagramOutlined, YoutubeOutlined } from '@ant-design/icons';
import  styles  from '../../styles/Home.module.css' 
import  commonStyles  from '../../styles/Common.module.css' 
import { Layout } from 'antd';
import Link from "next/link";

const { Header, Footer, Sider, Content } = Layout;
// function MainBanner() {
    const NewsSection = (props) => {
    
    const { Meta } = Card;
    return (
        <div className={commonStyles.wrapper}>
            <Divider orientation="left" style={{fontSize: '26px'}}>NEWS</Divider>
            <Row gutter={16}>
                <Col span={8}>
                    <Link href="/news/global">
                        <div className={commonStyles.cardTypeOne} style={{backgroundImage: `url('./imagethree.jpg')`}}>
                            <div className={commonStyles.cardTypeOneInner}>
                                <h1>GLOBAL</h1>
                                <p>News accross the world</p>
                            </div>
                        </div>
                    </Link>
                </Col>
                <Col span={8}>
                    <Link href="/news/elonmusk">
                        <div className={commonStyles.cardTypeOne} style={{backgroundImage: `url('./imagesix.jpg')`}}>
                            <div className={commonStyles.cardTypeOneInner}>
                                <h1>FUTURE</h1>
                                <p>Upcomming Suprises</p>
                            </div>
                        </div>
                    </Link>
                </Col>
                <Col span={8}>
                    <Link href="/news/techs">
                    <div className={commonStyles.cardTypeOne} style={{backgroundImage: `url('./imageseven.jpg')`}}>
                            <div className={commonStyles.cardTypeOneInner}>
                                <h1>TECH</h1>
                                <p>Tech Inovation</p>
                            </div>
                        </div>
                    </Link>
                </Col>
            </Row>
        </div>
    )}

export default NewsSection