import React from 'react'
import { Row, Col, Button, Carousel, Layout } from 'antd';
import { FacebookOutlined, TwitterOutlined, InstagramOutlined, YoutubeOutlined } from '@ant-design/icons';
import  styles  from '../../styles/Home.module.css' 
import  commonStyles  from '../../styles/Common.module.css' 
import Link from "next/link";

const { Header, Footer, Sider, Content } = Layout;
    const MainBanner = (props) => {
    const styling = {
        width:"100%",
        border: '1px solid #fff',
    }
    const subBanner = {
        width:"100%",
        border: '3px solid #fff',
        height: '50%'
    }
    const contentStyle = {
        height: '294px',
        color: '#fff',
        lineHeight: '284px',
        textAlign: 'center',
        backgroundImage: `url('./nature.jpg')`,
        backgroundPosition: 'center',
        backgroundSize: 'cover'
    };
    return (
            <Row className={styles.banner}  justify="space-between" align="middle" >
                <Col className={commonStyles.h100} style={styling}  span={12} >
                  <Carousel>
                      <div>
                          <Link href="/news/concert">
                            <div className={commonStyles.cardTypeOne} style={{backgroundImage: `url('./imagefive.jpg')`}}>
                                    <div className={commonStyles.cardTypeOneInner}>
                                        <h1>Music FEST</h1>
                                        <p>World's Biggest Music Festival</p>
                                    </div>
                                </div>
                          </Link>
                      </div>
                      <div>
                          <Link href="/news/virtualreality">
                            <div className={commonStyles.cardTypeOne} style={{backgroundImage: `url('imagefour.jpg')`}}>
                                    <div className={commonStyles.cardTypeOneInner}>
                                        <h1>Advanced Tech</h1>
                                        <p>Virtual Reality (VR)</p>
                                    </div>
                                </div>
                          </Link>
                      </div>
                      <div>
                            <Link href="/news/football">
                                <div className={commonStyles.cardTypeOne} style={{backgroundImage: `url('./imageone.jpg')`}}>
                                    <div className={commonStyles.cardTypeOneInner}>
                                        <h1>Sports</h1>
                                        <p>Football lovers save the date!</p>
                                    </div>
                                </div>
                          </Link>
                      </div>
                      <div>
                    <div className={commonStyles.cardTypeOne} style={{backgroundImage: `url('./imagethree.jpg')`}}>
                            <div className={commonStyles.cardTypeOneInner}>
                                <h1>Global</h1>
                                <p>Current Situation of USA</p>
                            </div>
                        </div>
                      </div>
                </Carousel>
                </Col>
                <Col className={commonStyles.h100} span={12} >
                    <Row  className={commonStyles.h100} >
                        <Col className={commonStyles.h50} span={24} >
                            <Link href="/news/marvel">
                                <div className={commonStyles.cardTypeOne} style={{backgroundImage: `url('./image35.jpg')`, height: '100%'}}>
                                    <div className={commonStyles.cardTypeOneInner}>
                                        <h1>Marvel</h1>
                                        <p>Marvel Universe is back</p>
                                    </div>
                                </div>
                            </Link>
                        </Col>
                        <Col className={commonStyles.h50} style={styling} span={24} >
                            <Link href="/news/autoexpo">
                                <div className={commonStyles.cardTypeOne} style={{backgroundImage: `url('./image36.jpg')`, height: '100%'}}>
                                    <div className={commonStyles.cardTypeOneInner}>
                                        <h1>Moto Expo</h1>
                                        <p>Auto Expo is a great platform to connect with the masses and showcase products to India and the world.</p>
                                    </div>
                                </div>
                            </Link>
                        </Col>
                    </Row>
                </Col>
            </Row>
    )}
    

export default MainBanner