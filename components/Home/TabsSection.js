import React from 'react'
import { Row, Tabs , Col, Divider, Card, Typography } from 'antd';
import  commonStyles  from '../../styles/Common.module.css' 

const { TabPane } = Tabs;
const { Link } = Typography;

function callback(key) {
  console.log(key);
}

const TabsSection = (props) => {
    
    const { Meta } = Card;
    return (
        <div className={commonStyles.wrapper}>
            <Divider orientation="left" style={{fontSize: '26px'}}>NEWS</Divider>
            <Row  gutter={16}>
                <Col span={16}>
                    <Tabs defaultActiveKey="1" onChange={callback}>
                        <TabPane tab="FASHION" style={{fontSize : '20px'}} key="1">
                            <Row gutter={10}>
                                <Col span={12}>
                                    <Link href="/news/fashion">
                                        <div className="cardTypeTwo">
                                            <div className={commonStyles.cardTypeTwoImage} style={{backgroundImage: `url('./imagetwo.jpg')`}}>
                                            </div>
                                            <div className={commonStyles.cardTypeTwoInner}>
                                                <p>FASHION</p>
                                                <h1>Fashion is an art form, a glimpse into someone's personality</h1>
                                                <div className={commonStyles.cardTypeTwoFooter + ' ' + commonStyles.alignSpaceBetweenCenter}>
                                                    <h5>Author</h5>
                                                    <span>June 3, 2020</span>
                                                </div>
                                            </div>
                                        </div>
                                    </Link>
                                </Col>
                                <Col span={12}>
                                    <Link href="/news/fashion">
                                        <div className="cardTypeTwo">
                                            <div className={commonStyles.cardTypeTwoImage} style={{backgroundImage: `url('./image15.jpg')`}}>
                                            </div>
                                            <div className={commonStyles.cardTypeTwoInner}>
                                                <p>FASHION</p>
                                                <h1>Fashion is an individual's statement of self-expression</h1>
                                                <div className={commonStyles.cardTypeTwoFooter + ' ' + commonStyles.alignSpaceBetweenCenter}>
                                                    <h5>Author</h5>
                                                    <span>June 3, 2020</span>
                                                </div>
                                            </div>
                                        </div>
                                    </Link>
                                </Col>
                                <Col span={12}>
                                    <Link href="/news/fashion">
                                        <div className="cardTypeTwo">
                                            <div className={commonStyles.cardTypeTwoImage} style={{backgroundImage: `url('./image16.jpg')`}}>
                                            </div>
                                            <div className={commonStyles.cardTypeTwoInner}>
                                                <p>FASHION</p>
                                                <h1>Fashion means to one person will be completely different than the next</h1>
                                                <div className={commonStyles.cardTypeTwoFooter + ' ' + commonStyles.alignSpaceBetweenCenter}>
                                                    <h5>Author</h5>
                                                    <span>June 3, 2020</span>
                                                </div>
                                            </div>
                                        </div>
                                    </Link>
                                </Col>
                                <Col span={12}>
                                    <Link href="/news/fashion">
                                        <div className="cardTypeTwo">
                                            <div className={commonStyles.cardTypeTwoImage} style={{backgroundImage: `url('./image19.jpeg')`}}>
                                            </div>
                                            <div className={commonStyles.cardTypeTwoInner}>
                                                <p>FASHION</p>
                                                <h1>Fashionista is someone who is focused on clothing</h1>
                                                <div className={commonStyles.cardTypeTwoFooter + ' ' + commonStyles.alignSpaceBetweenCenter}>
                                                    <h5>David Lee</h5>
                                                    <span>June 3, 2020</span>
                                                </div>
                                            </div>
                                        </div>
                                    </Link>
                                </Col>
                            </Row>
                        </TabPane>
                        <TabPane tab="LIFE STYLE" key="2">
                            <Row gutter={10}>
                                <Col span={12}>
                                    <Link href="/news/lifestyle">
                                        <div className="cardTypeTwo">
                                            <div className={commonStyles.cardTypeTwoImage} style={{backgroundImage: `url('./image25.jpg')`}}>
                                            </div>
                                            <div className={commonStyles.cardTypeTwoInner}>
                                                <p>LIFE STYLE</p>
                                                <h1>Lifestyle is the way you live including your style</h1>
                                                <div className={commonStyles.cardTypeTwoFooter + ' ' + commonStyles.alignSpaceBetweenCenter}>
                                                    <h5>David Lee</h5>
                                                    <span>June 3, 2020</span>
                                                </div>
                                            </div>
                                        </div>
                                    </Link>
                                </Col>
                                <Col span={12}>
                                    <Link href="/news/lifestyle">
                                        <div className="cardTypeTwo">
                                            <div className={commonStyles.cardTypeTwoImage} style={{backgroundImage: `url('./image20.jpg')`}}>
                                            </div>
                                            <div className={commonStyles.cardTypeTwoInner}>
                                                <p>LIFE STYLE</p>
                                                <h1>Lifestyle is the interests, opinions...</h1>
                                                <div className={commonStyles.cardTypeTwoFooter + ' ' + commonStyles.alignSpaceBetweenCenter}>
                                                    <h5>David Lee</h5>
                                                    <span>June 3, 2020</span>
                                                </div>
                                            </div>
                                        </div>
                                    </Link>
                                </Col>
                                <Col span={12}>
                                    <Link href="/news/lifestyle">
                                        <div className="cardTypeTwo">
                                            <div className={commonStyles.cardTypeTwoImage} style={{backgroundImage: `url('./image21.jpg')`}}>
                                            </div>
                                            <div className={commonStyles.cardTypeTwoInner}>
                                                <p>LIFE STYLE</p>
                                                <h1>The broader sense of lifestyle as a "way or style of living"</h1>
                                                <div className={commonStyles.cardTypeTwoFooter + ' ' + commonStyles.alignSpaceBetweenCenter}>
                                                    <h5>David Lee</h5>
                                                    <span>June 3, 2020</span>
                                                </div>
                                            </div>
                                        </div>
                                    </Link>
                                </Col>
                                <Col span={12}>
                                    <Link href="/news/lifestyle">
                                        <div className="cardTypeTwo">
                                            <div className={commonStyles.cardTypeTwoImage} style={{backgroundImage: `url('./image23.jpg')`}}>
                                            </div>
                                            <div className={commonStyles.cardTypeTwoInner}>
                                                <p>LIFE STYLE</p>
                                                <h1>Lifestyle is a combination of determining intangible</h1>
                                                <div className={commonStyles.cardTypeTwoFooter + ' ' + commonStyles.alignSpaceBetweenCenter}>
                                                    <h5>David Lee</h5>
                                                    <span>June 3, 2020</span>
                                                </div>
                                            </div>
                                        </div>
                                    </Link>
                                </Col>
                            </Row>
                        </TabPane>
                        <TabPane tab="EVENTS" key="3">
                            <Row  gutter={10}>
                                <Col span={12}>
                                    <div className="cardTypeTwo">
                                        <div className={commonStyles.cardTypeTwoImage} style={{backgroundImage: `url('./image27.jpg')`}}>

                                        </div>
                                        <div className={commonStyles.cardTypeTwoInner}>
                                            <p>EVENTS</p>
                                            <h1>Music fest aims to be COVID-19 </h1>
                                            <div className={commonStyles.cardTypeTwoFooter + ' ' + commonStyles.alignSpaceBetweenCenter}>
                                                <h5>David Lee</h5>
                                                <span>June 3, 2020</span>
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                                <Col span={12}>
                                    <div className="cardTypeTwo">
                                        <div className={commonStyles.cardTypeTwoImage} style={{backgroundImage: `url('./image29.jpg')`}}>

                                        </div>
                                        <div className={commonStyles.cardTypeTwoInner}>
                                            <p>EVENTS</p>
                                            <h1>Great Moments in TV Music History: Alice Cooper</h1>
                                            <div className={commonStyles.cardTypeTwoFooter + ' ' + commonStyles.alignSpaceBetweenCenter}>
                                                <h5>David Lee</h5>
                                                <span>June 3, 2020</span>
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                                <Col span={12}>
                                    <div className="cardTypeTwo">
                                        <div className={commonStyles.cardTypeTwoImage} style={{backgroundImage: `url('./imagefive.jpg')`}}>

                                        </div>
                                        <div className={commonStyles.cardTypeTwoInner}>
                                            <p>EVENTS</p>
                                            <h1>Red Metal Music Fest 2 to raise money</h1>
                                            <div className={commonStyles.cardTypeTwoFooter + ' ' + commonStyles.alignSpaceBetweenCenter}>
                                                <h5>David Lee</h5>
                                                <span>June 3, 2020</span>
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                                <Col span={12}>
                                    <div className="cardTypeTwo">
                                        <div className={commonStyles.cardTypeTwoImage} style={{backgroundImage: `url('./image28.jpg')`}}>

                                        </div>
                                        <div className={commonStyles.cardTypeTwoInner}>
                                            <p>EVENTS</p>
                                            <h1>Concert and Fests</h1>
                                            <div className={commonStyles.cardTypeTwoFooter + ' ' + commonStyles.alignSpaceBetweenCenter}>
                                                <h5>David Lee</h5>
                                                <span>June 3, 2020</span>
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </TabPane>
                    </Tabs>
                </Col>
                <Col span={8} style={{border:'2px solid #f0f0f0', padding:'15px'}}>
                    <Divider orientation="left" style={{fontSize: '22px', marginTop: '0'}}>NEWS</Divider>
                    <Row gutter={16}>
                        <Col span={24}>
                            <Link href="/news/retail">
                                <div className={commonStyles.alignSpaceBetweenCenter + ' ' + commonStyles.mb15}>
                                    <div className={commonStyles.cardTypeThreeImage} style={{backgroundImage: `url('./image37.jpg')`}}></div>
                                        <div className={commonStyles.cardTypeThreeInner}>
                                            <p>RETAIL</p>
                                            <h1>Retailers satisfy demand identified through a supply chain.</h1>
                                        </div>
                                </div>
                            </Link>
                        </Col>
                        <Col span={24}>
                            <Link href="/news/fashion">
                                <div className={commonStyles.alignSpaceBetweenCenter + ' ' + commonStyles.mb15}>
                                    <div className={commonStyles.cardTypeThreeImage} style={{backgroundImage: `url('./image17.jpg')`}}></div>
                                        <div className={commonStyles.cardTypeThreeInner}>
                                            <p>FASHION</p>
                                            <h1>Fashion is an art form, a glimpse into someone's personality.</h1>
                                        </div>
                                </div>
                            </Link>
                        </Col>
                        <Col span={24}>
                            <Link href="/news/sports">
                                <div className={commonStyles.alignSpaceBetweenCenter + ' ' + commonStyles.mb15}>
                                    <div className={commonStyles.cardTypeThreeImage} style={{backgroundImage: `url('./imageone.jpg')`}}></div>
                                        <div className={commonStyles.cardTypeThreeInner}>
                                            <p>SPORTS</p>
                                            <h1>Sports is the ultimate destination for Sports fans from around the World.</h1>
                                        </div>
                                </div>
                            </Link>
                        </Col>
                        <Col span={24}>
                            <Link href="/news/tech">
                                <div className={commonStyles.alignSpaceBetweenCenter + ' ' + commonStyles.mb15}>
                                    <div className={commonStyles.cardTypeThreeImage} style={{backgroundImage: `url('./imagefour.jpg')`}}>
                                    </div>
                                    <div className={commonStyles.cardTypeThreeInner}>
                                        <p>TECH</p>
                                        <h1>The latest tech news about the world's best hardware, apps, and much more.</h1>
                                    </div>
                                </div>
                            </Link>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </div>
    )}

export default TabsSection