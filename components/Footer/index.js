import React from 'react'
import Image from 'next/image'
import footerStyles from '../../styles/Footer.module.css';

function Footer() {
    return(
        <div className={footerStyles.footer} style={{backgroundImage: `url('./image32.jpg')`}}>
            <div className={footerStyles.footerInner}>
                <Image
                    src="/vercel.svg"
                    alt="Picture of the Logo"
                    width={150}
                    height={75}
                />
                <p>Newspapers provide information and general knowledge. Newspapers provide news about a country's economic situation, sports, games, entertainment, trade and commerce. Reading newspaper makes a good habit and it is already part of the modern life.</p>
                <p>Contact us : <a> contact@gmail.com</a></p>
            </div>
        </div>
    )
}

export default Footer