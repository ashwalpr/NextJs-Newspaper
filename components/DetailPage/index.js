import React, {useContext} from 'react'
import { Divider } from 'antd';
import  commonStyles  from '../../styles/Common.module.css' 

function DetailPage({value}) {
    return(
        <div>
            <div style={{backgroundImage: `url('${value.urlToImage}')`, width: '100%', height: '250px', backgroundPosition: 'center', backgroundSize: 'cover'}}>
            </div>
            <div className={commonStyles.wrapper}>
                <Divider orientation="left" style={{fontSize: '26px'}}>{value.title}</Divider>
                <p>{value.content}</p>
                <p>{value.description}</p>
            </div>
        </div>
    )
}

export default DetailPage