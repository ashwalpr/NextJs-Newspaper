import React from 'react'
import { Row , Col, Divider } from 'antd';
import  commonStyles  from '../../styles/Common.module.css' 
import Link from "next/link";

const getDataFromNews = async () => {
    try{
        let response = await fetch('/api/fetchNews').then(res => res.json())
        console.log('response', response);
    } catch(e){
        console.log('error from client side api call', e)
    } finally{
        // Disable loader
    }
}



function ListingPage(props) {
    return(
        <div>
            <div style={{backgroundImage: `url('/image34.jpg')`, width: '100%', height: '250px', backgroundPosition: 'center', backgroundSize: 'cover'}}>
            </div>
            <div className={commonStyles.wrapper}>
                <Divider orientation="left" style={{fontSize: '26px'}}>NEWS</Divider>
                <Row gutter={[8, 8]}>
                    {props.value.articles.map((item, index) => (
                        <Col className={commonStyles.h100}  span={12} >
                            <Link href={`/newsDetail/${index}`}>
                                <div className={commonStyles.cardTypeFour}>
                                    <div className={commonStyles.cardTypeFourImage} style={{backgroundImage: `url('${item.urlToImage}')`}}>
                                    </div>
                                    <div className={commonStyles.cardTypeFourInner}>
                                        <h1>{item.title}</h1>
                                        <div className={commonStyles.cardTypeFourFooter + ' ' + commonStyles.alignSpaceBetweenCenter}>
                                            <h5>{item.author}</h5>
                                        </div>
                                        <p>{item.description}</p>
                                    </div>
                                </div>
                            </Link>
                        </Col>
                    ))}
                </Row>
            </div>
        </div>
    )
}

export default ListingPage