import React from 'react'
import { Row, Col, Button } from 'antd';
import { FacebookOutlined, TwitterOutlined, InstagramOutlined, YoutubeOutlined } from '@ant-design/icons';
import  styles  from '../../styles/Header.module.css' 
import  commonStyles  from '../../styles/Common.module.css' 

function TopHeader() {
    return(
        <div className={styles.topHeader}>
            <Row   justify="space-between" align="middle" className={commonStyles.wrapper}>
                <Col className={styles.topHeaderLeft}>
                    <Button type="text" size="small">Monday, March 1, 2021</Button>
                    <Button type="text" size="small">Sign in / Join</Button>
                    <Button type="text" size="small">Blog</Button>
                    <Button type="text" size="small">Forum</Button>
                    <Button type="text" size="small">Buy now!</Button>
                </Col>
                <Col className={styles.topHeaderLeft}>
                    <Button type="text" shape="round" icon={<FacebookOutlined />}  />
                    <Button type="text" shape="round" icon={<TwitterOutlined />} size='small' />
                    <Button type="text" shape="round" icon={<YoutubeOutlined />} size='small' />
                    <Button type="text" shape="round" icon={<InstagramOutlined />} size='small' />
                </Col>
            </Row>
        </div>
    )
}

export default TopHeader