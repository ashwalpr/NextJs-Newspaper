import React from 'react'
import { Menu, Input  } from 'antd';
import { FireFilled, MobileFilled, AppstoreOutlined, DribbbleCircleFilled, SettingOutlined, HomeOutlined } from '@ant-design/icons';
import commonStyles from '../../styles/Common.module.css'
import Link from "next/link";
import { useRouter } from 'next/router';
const { Search } = Input;
const onSearch = (value, router) => {
    router.push({
    pathname: `/news/${value}`
    })
};



function NavBar() {

const router = useRouter()
    return(
        <div style={{backgroundImage: `url('./imageeight.jpg')`, boxShadow: 'inset 0px 0px 0px 1000px #000000bd', padding: '5px'}}>
            <div className={commonStyles.wrapper + ' ' + commonStyles.alignSpaceBetweenCenter } style={{boxShadow: '0px 0px 9px -2px #fff', padding: '5px'}}>
                <Menu  mode="horizontal"  style={{backgroundColor: 'transparent', fontWeight: '600', color: '#fff', border: '0', width: '100%'}}>
                    <Menu.Item key="home" icon={<HomeOutlined />}>
                        <Link href="/">
                        <span style={{color:'#fff'}}>
                            HOME
                        </span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="fasion" icon={<FireFilled />}>
                        <Link href="/news/fashion">
                            <span style={{color:'#fff'}}>
                                FASHION
                            </span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="gadgets" icon={<MobileFilled />}>
                        <Link href="/news/gadgets">
                            <span style={{color:'#fff'}}>
                                GADGETS
                            </span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="sports" icon={<DribbbleCircleFilled />}>
                        <Link href="/news/sports">
                            <span style={{color:'#fff'}}>
                                SPORTS
                            </span>
                        </Link>
                    </Menu.Item>
                </Menu>
                <Search style={{width: '300px'}} placeholder="Search" onSearch={(value) => onSearch(value, router)}  />
            </div>
        </div>
    )
}

export default NavBar