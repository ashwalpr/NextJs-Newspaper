import React from 'react'
import Image from 'next/image'
import commonStyles from '../../styles/Common.module.css'
// import { Image } from 'antd';

function MainHeader() {
    return(
        <div className={commonStyles.wrapper}>
            <Image
                src="/vercel.svg"
                alt="Picture of the Logo"
                width={150}
                height={75}
            />
        </div>
    )
}

export default MainHeader