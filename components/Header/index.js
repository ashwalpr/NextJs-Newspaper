import React from 'react'
import TopHeader from './TopHeader' 
import MainHeader from './MainHeader' 
import NavBar from './NavBar' 

function Header() {
    return(
        <div>
            <TopHeader />
            <MainHeader />
            <NavBar />
        </div>
    )
}

export default Header