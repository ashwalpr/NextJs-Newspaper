import React, {useContext, useEffect} from 'react'
import DetailPage from '../../components/DetailPage'
import {NewsContext} from '../_app'


function NewsDetail() {
    const {initialState} = useContext(NewsContext)
    return(
        (initialState && initialState.data) ? <DetailPage value={initialState.data.articles[0]}/> : <p>Sorry</p>
    )
}

export default NewsDetail