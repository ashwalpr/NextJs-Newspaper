import React, {useContext, useEffect} from 'react'
import ListingPage from '../../components/ListingPage'
import { NewsContext } from '../../pages/_app'


export async function getServerSideProps({query: { id }}) {
    const res = await fetch(`http://newsapi.org/v2/everything?q=${id}&from=2021-03-01&sortBy=publishedAt&apiKey=c3bc3898f4574115be393803900512d9`);
    const data = await res.json();
    return { props: { data } }
}

function DynamicListingPage({data}) {
    const {initialState, setInitialState} = useContext(NewsContext)
    useEffect(() => {
        setInitialState({...initialState, data})
    }, [])
    return(
        data ? <ListingPage  value={data}/> : <p>Sorry, Unable to load</p>
    )
}

export default DynamicListingPage