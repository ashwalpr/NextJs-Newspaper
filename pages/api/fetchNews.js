export default async (req, res) => {
  const {
    query: { id, name },
    method,
  } = req

  const resp = await fetch(`http://newsapi.org/v2/everything?q=${id}&from=2021-01-28&sortBy=publishedAt&apiKey=37ec0a66b0024accaeaacd892ce2152b`);
  const data = await resp.json();
  res.status(200).json(data)
}
