import React, {useState} from 'react'
import "antd/dist/antd.css";
import '../styles/globals.css'
import Header from '../components/Header/index'
import Footer from '../components/Footer/index'

export const NewsContext = React.createContext(
  {initialState : {}, setInitialState: () => {

  }}
)


function MyApp({ Component, pageProps }) {
  
  const [initialState, setInitialState] = useState({})
  return (
    <>
    {console.log('Just to text', initialState)};
      <Header />
        <NewsContext.Provider value={{initialState, setInitialState}}>
          <Component {...pageProps} />
        </NewsContext.Provider>
      <Footer />
    </>
  )
}

export default MyApp
